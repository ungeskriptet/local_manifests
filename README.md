# local_manifests

Note for Fire TV Stick 4K:
> Comment out these lines in `build/core/config.mk` like this to avoid build error:
> ```
> # ###############################################################
> # Set up final options.
> # ###############################################################
>
> #ifneq ($(COMMON_GLOBAL_CFLAGS)$(COMMON_GLOBAL_CPPFLAGS),)
> #$(warning COMMON_GLOBAL_C(PP)FLAGS changed)
> #$(info *** Device configurations are no longer allowed to change the global flags.)
> #$(info *** COMMON_GLOBAL_CFLAGS: $(COMMON_GLOBAL_CFLAGS))
> #$(info *** COMMON_GLOBAL_CPPFLAGS: $(COMMON_GLOBAL_CPPFLAGS))
> #$(error bailing...)
> #endif
> ```
> Source: https://forum.xda-developers.com/t/error-building-cm14-1-for-bacon.3493918/#post-69892170
